#Download base image ubuntu 16.04
FROM ubuntu:16.04
MAINTAINER Chwabo Carl

#Update Ubuntu Software repository
RUN apt-get update

RUN apt-get -y install curl
RUN curl -ssL http://get.gazebosim.org | sh
RUN gazebo